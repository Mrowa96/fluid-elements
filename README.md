# Fluid Elements - DEPRECATED
I'm happy to present the library, which simplified process of creating new web application.

## Getting started
To use this library, you must include it on your website, in ```<head> ``` section, 
and in the next step you can create new instance of modules, 
but remember about wrapping your JavaScript code into onload function.
Example: 
```
<head> 
    <script src="fluid-core.js"></script>
    <script>
``` 
```javascript
        window.addEventListener("load", function(){
            var modal = new Fluid.Modal();
        });
``` 
```
    </script>
</head>
```

## Documentation
### Modal
1. Overview
    Modal module presents the simples way to display some data to user in good looking package.
    You can personalize your modals, like you want. You can even change actions(close|hide|minimize|maximize) block position
2. Configuration
    * show - Display modal after initialization or after n seconds. Values: true | false | n (seconds). Default: true.
    * hide - Hide modal after initialization or after n seconds. Values: true | false | n (seconds). Default: false.
    * close - Close modal after initialization or after n seconds. Values: true | false | n (seconds). Default: false.
    * header - Sets up header block of modal. Values: String | HTMLElement | false. Default: String.
    * content - Sets up content block of modal. Values: String | HTMLElement | false. Default: String.
    * contentAjax - Get content from provided url, and inject it to modal content block. Values: String(url) | false. Default: false.
    * footer - Sets up footer block of modal. Values: String | HTMLElement | false. Default: false.
    * create - Create modal from scratch. Values: true | false. Default: false.
    * actions - Add actions to modal. Values: object(events, parent). Default: ```{
                                                                     events: "hide",
                                                                     parent: "header"
                                                                 }```
    * animations - Add animations to modal. Values: true | false. Default: true.
    * type - Set up type of modal. Values: String("standard | dialog | alert | prompt"). Default: "standard".
    * dim - Create dim, when modal is showed. Values: true | false. Default: false.
3. Important information
    * Modals can be hided or closed. Difference is, that closed modal is removed from DOM tree and cannot be displayed again.
4. Example
    ```javascript
    var modal = new Fluid.Modal();
    ```

