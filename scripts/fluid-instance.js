(function() {
    var FI = {
        wrapper: document.getElementsByClassName("wrapper")[0],
        started: false,
        elements: {},
        temporary: {},
        dataName: 'FI',
        Lib: {},

        init: function () {
            window.addEventListener("load", function() {
                if (Fluid && FI.started === false) {
                    Fluid.Loader.load(FI);

                    FI.Lib = new FI.Lib();
                    window.FI = FI;

                    console.info("Fluid Instance started.");
                }
            }, false);
        },
        onLoad: function(){
            window.addEventListener("load", function() {

            });
        }
    };

    FI.Lib = function(){};
  
    FI.init();
    FI.onLoad();
}());
